﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EHEC_Server
{
    /// <summary>
    /// This is a Singleton implementation for the need of one single db connection
    /// </summary>
    public partial class Global
    {
        private static Global globalInstance = null;
        private static EHEC_DBEntities dbAccess = null;

        public static Global GlobalInstance {
            get {
                if (globalInstance == null)
                {
                    globalInstance = new Global();
                }
                return globalInstance;
            }
        }

        public EHEC_DBEntities DbAccess
        {
            get {
                if (dbAccess == null)
                {
                    dbAccess = new EHEC_DBEntities();
                }
                return dbAccess;
            }
        }
    }
}