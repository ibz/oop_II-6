﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace EHEC_Server
{
    public partial class cluster : System.Web.UI.Page
    {
        protected string Clusterdata { get; set; }
        protected string ClusterNodes { get; set; }
        protected string ClusterEdges { get; set; }

        ClusterConverter cc = new ClusterConverter();
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Diagnostics.Trace.WriteLine(" --i-- Cluster Pageload: generating page...");
            //Clusterdata = JsonConvert.SerializeObject(cc.WriteJson());
            ClusterNodes = cc.WriteJsonNodes();
            ClusterEdges = cc.WriteJsonEdges();
            //ClusterNodes = JsonConvert.SerializeObject(clusterNodes.GetNodes());
            //ClusterEdges = JsonConvert.SerializeObject(clusterEdges.GetEdges());
        }
        public void Button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Trace.WriteLine(" --i-- Cluster Click: refreshing data...");
            //Clusterdata = JsonConvert.SerializeObject(cc.WriteJson());
            ClusterNodes = cc.WriteJsonNodes();
            ClusterEdges = cc.WriteJsonEdges();
            //ClusterNodes = JsonConvert.SerializeObject(clusterNodes.GetNodes());
            //ClusterEdges = JsonConvert.SerializeObject(clusterEdges.GetEdges());
        }
        public void Refresh()
        {
            System.Diagnostics.Trace.WriteLine(" --i-- Cluster Refresh: refreshing page...");
            //ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "text", "RefreshPage()", true);
            //ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('Hello World');", true);
            //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "RefreshPage()", true);
            //Response.Redirect(Request.RawUrl);
            //Response.Redirect(Request.Url.AbsoluteUri);
            //Server.TransferRequest(Request.Url.AbsolutePath, false);
        }

    }
}