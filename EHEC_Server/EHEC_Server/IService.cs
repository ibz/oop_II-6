﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EHEC_Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
    [ServiceContract]
    public interface IService
    {

        [OperationContract]
        Doctor WriteDoctor(Doctor doctor);

        [OperationContract]
        Patient WritePatient(Patient patient);

        [OperationContract]
        Origin WriteOrigin(Origin origin);

        [OperationContract]
        Exam WriteExam(Exam exam);

        [OperationContract]
        Origin_Exam WriteOrigin_Exam(Origin_Exam origin_exam);

        [OperationContract]
        Result WriteResult(Result result);

        [OperationContract]
        City WriteCity(City city);



        [OperationContract]
        List<Result> GetResults();

        [OperationContract]
        List<City> GetCities();



        [OperationContract]
        bool CreateRandomData(int valueToCreate);

        [OperationContract]
        String Ping();

        [OperationContract]
        void ReloadCluster();
    }
}
