﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;


namespace EHEC_Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the 
    //       class name "Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select 
    //       Service.svc or Service.svc.cs at the Solution Explorer and start debugging.
    public class Service : IService
    {

        Doctor dataaccess_doctor = new Doctor();
        //private static List<Doctor> Doctors = new List<Doctor>();
        Patient dataaccess_patient = new Patient();
        Result dataaccess_result = new Result();
        Origin dataaccess_origin = new Origin();
        Origin_Exam dataaccess_origin_exam = new Origin_Exam();
        Exam dataaccess_exam = new Exam();
        City dataaccess_city = new City();


        public List<Doctor> GetDoctors()
        {
            return dataaccess_doctor.GetAllDoctors();
        }

        public List<Patient> GetPatients()
        {
            return dataaccess_patient.GetAllPatients();
        }
        public List<Result> GetResults()
        {
            return dataaccess_result.GetAllResults();
        }
        public List<City> GetCities()
        {
            return dataaccess_city.GetAllCities();
        }

        public Doctor WriteDoctor(Doctor doctor)
        {
            return dataaccess_doctor.CreateDoctor(doctor);
        }

        public Patient WritePatient(Patient patient)
        {
            return dataaccess_patient.CreatePatient(patient);
        }

        public Origin WriteOrigin(Origin origin)
        {
            return dataaccess_origin.CreateOrigin(origin);
        }
        public Origin_Exam WriteOrigin_Exam(Origin_Exam origin_exam)
        {
            return dataaccess_origin_exam.CreateOrigin_Exam(origin_exam);
        }
        public Exam WriteExam(Exam exam)
        {
            return dataaccess_exam.CreateExam(exam);
        }

        public Result WriteResult(Result result)
        {
            return dataaccess_result.CreateResult(result);
        }

        public City WriteCity(City city)
        {
            return dataaccess_city.CreateCity(city);
        }

        public bool CreateRandomData(int valueToCreate)
        {
            return GenerateDataBuilder.CreateRandomData(valueToCreate);
        }

        private readonly string ping = "pong";
        public string Ping()
        {
            return ping;
        }

        public void ReloadCluster()
        {
            cluster c = new cluster();
            c.Refresh();
        }
    }
}
