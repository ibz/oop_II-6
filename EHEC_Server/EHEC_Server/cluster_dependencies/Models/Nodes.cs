﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EHEC_Server
{
    public class Node
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string Title { get; set; }
        public string Group { get; set; }
        public Node() { }
        public List<Node> GetNodes()
        {
            List<Node> X = new List<Node>();
            X.AddRange(GetClusterCities());
            X.AddRange(GetClusterPatients());
            X.AddRange(GetClusterResults());
            return X;
        }
        public List<Node> GetClusterCities()
        {
            City city = new City();
            List<City> cities = new List<City>();
            cities = city.GetAllCities();
            List<Node> clusterCities = new List<Node>();
            foreach (City cityElement in cities)
            {
                Node clusterCity = new Node
                {
                    Id = cityElement.CityUid,
                    Title = "origin",
                    Group = "origin",
                    Label = cityElement.CityName,
                };
                clusterCities.Add(clusterCity);
            };
            return clusterCities;
        }
        public List<Node> GetClusterPatients()
        {
            Patient patient = new Patient();
            List<Patient> patients = new List<Patient>();
            patients = patient.GetAllPatients();
            List<Node> clusterPatients = new List<Node>();
            foreach (Patient element in patients)
            {
                Node clusterPatient = new Node
                {
                    Id = element.PatientUid,
                    Title = "patient",
                    Group = "patient",
                    Label = element.FirstName + " " + element.LastName
                };
                clusterPatients.Add(clusterPatient);
            }
            return clusterPatients;
        }
        public List<Node> GetClusterResults()
        {
            Result result = new Result();
            List<Result> results = new List<Result>();
            results = result.GetAllResults();
            List<Node> clusterResults = new List<Node>();
            foreach (Result element in results)
            {
                Node clusterResult = new Node
                {
                    Id = element.ResultUid,
                    Title = "result",
                    Group = "result",
                    Label = element.Name
                };
                clusterResults.Add(clusterResult);
            };
            return clusterResults;
        }

    }
}
