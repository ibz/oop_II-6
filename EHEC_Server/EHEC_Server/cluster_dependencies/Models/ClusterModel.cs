﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EHEC_Server
{
    public class ClusterModel
    {
        public string Comment { get; set; }
        public List<Node> Nodes { get; set; }
        public List<Edge> Edges { get; set; }
    }
}