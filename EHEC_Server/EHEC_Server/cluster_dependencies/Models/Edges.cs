﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EHEC_Server
{
    public class Edge
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Label { get; set; }
        public Edge() { }
        public List<Edge> GetEdges()
        {
            List<Edge> X = new List<Edge>();
            X.AddRange(GetClusterRelationsPatientsOrigins());
            X.AddRange(GetClusterRelationsPatientsResults());
            return X;
        }
        public List<Edge> GetClusterRelationsPatientsOrigins()
        {
            Exam exam = new Exam();
            List<Exam> exams = new List<Exam>();

            Origin_Exam originExam = new Origin_Exam();
            List<Origin_Exam> originExams = new List<Origin_Exam>();

            Origin origin = new Origin();
            List<Origin> origins = new List<Origin>();

            City city = new City();
            List<City> cities = new List<City>();

            Patient patient = new Patient();
            List<Patient> patients = new List<Patient>();
            patients = patient.GetAllPatients();
            
            List<Edge> clusterEdges = new List<Edge>();

            foreach (Patient patientElement in patients)
            {
               // System.Diagnostics.Trace.WriteLine(" --i-- GetClusterRelationsPatientsOrigins-patientElement.PatientId: " + patientElement.PatientId);
                exams = exam.GetExamsByPatientId(patientElement.PatientId);
                foreach (Exam examElement in exams)
                {
                    //System.Diagnostics.Trace.WriteLine(" --i-- GetClusterRelationsPatientsOrigins-examElement.ExamId: " + examElement.ExamId);
                    originExams.Clear();
                    originExams.AddRange(originExam.GetOriginExamsByExamId(examElement.ExamId));
                    //System.Diagnostics.Trace.WriteLine(" --i-- GetClusterRelationsPatientsOrigins-originExams.Count: " + originExams.Count);
                    foreach (Origin_Exam origin_exam_element in originExams)
                    {
                        //System.Diagnostics.Trace.WriteLine(" --i-- GetClusterRelationsPatientsOrigins-origin_exam_element.Origin_ExamId: " + origin_exam_element.Origin_ExamId);
                        origin = origin.GetOriginByOriginId(origin_exam_element.OriginId);
                        //System.Diagnostics.Trace.WriteLine(" --i-- GetClusterRelationsPatientsOrigins-origin.OriginId: " + origin.OriginId);
                        city = city.GetCityByCityId(origin.CityId);
                        //System.Diagnostics.Trace.WriteLine(" --i-- GetClusterRelationsPatientsOrigins-city.CityId: " + city.CityId);

                        Edge clusterEdge = new Edge
                        {
                            Label = "Angesteckt in ",
                            From = patientElement.PatientUid,
                            To = city.CityUid
                        };
                        clusterEdges.Add(clusterEdge);
                    }
                }
            }
            return clusterEdges;
        }
        public List<Edge> GetClusterRelationsPatientsResults()
        {
            Patient patient = new Patient();
            Result result = new Result();
            Exam exam = new Exam();
            List<Exam> exams = new List<Exam>();
            exams = exam.GetAllExams();
            List<Edge> clusterEdges = new List<Edge>();
            foreach (Exam exam_element in exams)
            {
                patient = patient.GetPatientIdById(exam_element.PatientId);
                result = result.GetResultsById(exam_element.ResultId);
                Edge clusterEdge = new Edge
                {
                    Label = "Wurde Angesteckt mit ",
                    From = patient.PatientUid,
                    To = result.ResultUid
                };
                clusterEdges.Add(clusterEdge);
            };
            return clusterEdges;
        }
        //public List<Edge> GetClusterRelationOrigin()
        //{
        //    Origin origin = new Origin();
        //    List<Origin> origins = new List<Origin>();
        //    origins = origin.GetAllOrigins();
        //    List<Edge> clusterOrigins = new List<Edge>();
        //    foreach (Origin element in origins)
        //    {
        //        Edge clusterOrigin = new Edge
        //        {
        //            Label = "Angesteckt in ",
        //            From = element.OriginUid,
        //            To = element.City
        //        };
        //        clusterOrigins.Add(clusterOrigin);
        //    };
        //    return clusterOrigins;
        //}
    }
    
}
    
