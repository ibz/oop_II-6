﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EHEC_Server;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace EHEC_Server
{
    public class ClusterConverter
    {
        protected string Json { get; set; }
        protected string JsonNodes { get; set; }
        protected string JsonEdges { get; set; }
        public JObject WriteJson()
        {
            var serializer = new JsonSerializer
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            var Json = JObject.FromObject(GetCluster(), serializer);
            System.IO.File.WriteAllText(@"C: \Users\novski\Desktop\cluster.json", JsonConvert.SerializeObject(Json));
            //System.IO.File.WriteAllText(@"C: \Users\Isi-C\Desktop\cluster.json", JsonConvert.SerializeObject(Json));
            return Json; 
        }
        public string WriteJsonNodes()
        {
            Node node = new Node();
            var serializer = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            var JsonNodes = JsonConvert.SerializeObject(node.GetNodes(), Formatting.Indented, serializer);
            //System.IO.File.WriteAllText(@"C: \Users\novski\Desktop\clusterNodes.json", JsonConvert.SerializeObject(JsonNodes));
            return JsonNodes;
        }
        public string WriteJsonEdges()
        {
            Edge edge = new Edge();
            var serializer = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            var JsonEdges = JsonConvert.SerializeObject(edge.GetEdges().ToList(), Formatting.Indented, serializer);
            //System.IO.File.WriteAllText(@"C: \Users\novski\Desktop\clusterEdges.json", JsonConvert.SerializeObject(JsonEdges));
            return JsonEdges;
        }
        private ClusterModel GetCluster()
        {
            Node clusterNode = new Node();
            Edge clusterEdge = new Edge();
            return new ClusterModel
            {
                Comment = "some comment",
                Nodes = clusterNode.GetNodes(),
                Edges = clusterEdge.GetEdges(),

            };
        }
        private NodeModel GetClusterNodes()
        {
            Node node = new Node();
            return new NodeModel
            {
                Nodes = node.GetNodes()
            };
        }
        private EdgeModel GetClusterEdges()
        {
            Edge edge = new Edge();
            return new EdgeModel
            {
                Edges = edge.GetEdges()
            };
        }
    }
}