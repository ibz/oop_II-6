﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EHEC_Server.cluster" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head runat="server">
        <title>CS-EHEC-II</title>
        <script type="text/javascript" src="cluster_dependencies/jquery/jquery-3.3.1.min.js"></script>  
        <link rel="stylesheet" type="text/css" href="cluster_dependencies/bootstrap/css/bootstrap.css" />
        <script type="text/javascript" src="cluster_dependencies/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="cluster_dependencies/visjs/dist/vis.js"></script>
        <link href="cluster_dependencies/visjs/dist/vis.css" rel="stylesheet" type="text/css" />
        <link href="cluster_dependencies/visjs/dist/vis-network.min.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            #mynetwork {
                width: 800px;
                height: 800px;
                border: 1px solid lightgray;
                background-color: black
            }
            #infomessage {
                font-family: Arial;
                font-weight:bold
            }
            #buttons {
                width: 119px;
            }
            #stabilisationBox {
                width: 175px;
            }
        </style>
    </head>
    <body>
        <div>
            <h1>EHEC Clusteranalysis</h1>
        </div>
        <form id="form1" runat="server">
            <%--<asp:ScriptManager ID="ScriptManager1" runat="server">
                <Services>
                    <asp:ServiceReference Path="~/cluster.aspx" />
                </Services>
            </asp:ScriptManager>
            <script type="text/javascript">
                function RefreshPage()
                {
                    window.location.reload();
                    console.log("reload..");
                }
            </script>--%>
            <asp:Button 
                ID="Button1" 
                runat="server" 
                Text="reload" 
                OnClick="Button1_Click"
                Font-Bold="true"
                ForeColor="DodgerBlue"
                Height="25"
                Width="80"
            />
        </form> 
        <br />
        <div id="mynetwork"></div>
        <script type="text/javascript"> <%-- <%=Clusterdata%> --%>
            // create an array with nodes
            var nodes = new vis.DataSet(<%=ClusterNodes%>);
            // create an array with edges
            var edges = new vis.DataSet(<%=ClusterEdges%>);
            // create a network
            var container = document.getElementById('mynetwork');
            // provide data in the normal fashion
            var data = {
              nodes: nodes,
              edges: edges
            };
            //var options = {};
            var options = {
                autoResize: false,
                height: '60%',
                width: '60%',
                physics: {
                    enabled: true,
                    forceAtlas2Based: {
                        gravitationalConstant: -50,
                        centralGravity: 0.01,
                        springConstant: 0.08,
                        springLength: 100,
                        damping: 0.4,
                        avoidOverlap: 1
                    },
                    maxVelocity: 50,
                    minVelocity: 0.1,
                    solver: 'forceAtlas2Based',
                    stabilization: {
                        enabled: true,
                        iterations: 2,
                        updateInterval: 100,
                        onlyDynamicEdges: true,
                        fit: true
                    },
                    timestep: 0.5,
                    adaptiveTimestep: true
                },
                layout: {
                    //randomSeed: undefined,
                    improvedLayout: true,
                    hierarchical: {
                        enabled: false,
                        levelSeparation: 250,
                        nodeSpacing: 500,
                        treeSpacing: 100,
                        blockShifting: true,
                        edgeMinimization: true,
                        parentCentralization: false,
                        direction: 'UD',        // UD, DU, LR, RL
                        sortMethod: 'directed'  // hubsize, directed
                    }
                },
                groups: {
                    result: {
                        shape: 'triangle',
                        size: 40,
                        color: {
                            background: 'red'
                        },
                        font: {
                        color: '#FFFFFF'
                        },
                        borderWidth: 1
                    },
                    origin: {
                        shape: 'star',
                        size: 40,
                        color: {
                            background: 'green'
                        },
                        font: {
                        color: '#FFFFFF'
                        },
                        borderWidth: 1
                    },
                    patient: {
                        shape: 'ellipse',
                        size: 60,
                        color: {
                            background: 'yellow'
                        },
                        font: {
                        color: '#111111'
                        },
                        borderWidth: 1
                    }
                },
                nodes: {
                    borderWidth: 1,
                    borderWidthSelected: 2,
                    font: {
                        size: 26
                    }
                },
                edges: {
                    hoverWidth: function (width) {return width+1;}
                }

            };

            // initialize your network!
            var network = new vis.Network(container, data, options);

        </script>
    </body>
</html>
