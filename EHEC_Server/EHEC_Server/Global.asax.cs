﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace EHEC_Server
{
    public partial class Global : System.Web.HttpApplication
    {
        private static List<string> Bacterias = new List<string>(new string[]
        {
            "EHEC-A", "EHEC-B", "EHEC-C", "EHEC-D", "EHEC-E", "EHEC-F"
        });
        private static List<Result> Results = new List<Result>();
        protected void Application_Start(object sender, EventArgs e)
        {
            // Create some Bacteriatypes on startup to populate dropdown in client. 
            for (int i = 0; i < Bacterias.Count(); i++)
            {
                Result result = new Result
                {
                    ResultUid = Guid.NewGuid().ToString(),
                    Name = Bacterias[i].ToString()
                };
                result.CreateResult(result);
            }
        }
        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }


    }
}
