﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;


namespace EHEC_Server
{
    public partial class Doctor
    {
        public Doctor CreateDoctor(Doctor doctor)
        {
            try
            {
                using (EHEC_DBEntities ctx = new EHEC_DBEntities())
                {
                    doctor.DoctorUid = Guid.NewGuid().ToString();
                    ctx.Doctors.Add(doctor);
                    ctx.SaveChanges();
                }
                return doctor;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(" --e-- Error in ServerCreateDoctor: " + e);
                return new Doctor();
            }
        }
        public List<Doctor> GetAllDoctors()
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                return ctx.Doctors.ToList();
            }
        }
    }
}