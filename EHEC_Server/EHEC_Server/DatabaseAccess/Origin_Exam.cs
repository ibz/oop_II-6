﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EHEC_Server
{
    public partial class Origin_Exam
    {
        public Origin_Exam CreateOrigin_Exam(Origin_Exam origin_exam)
        {
            try
            {
                using (EHEC_DBEntities ctx = new EHEC_DBEntities())
                {
                    origin_exam.Origin_ExamUid = Guid.NewGuid().ToString();
                    ctx.Origin_Exam.Add(origin_exam);
                    ctx.SaveChanges();
                }
                return origin_exam;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(" --e-- Error in ServerCreateOriginExam: " + e);
                return new Origin_Exam();
            }
        }
        public List<Origin_Exam> GetAllOrigin_Exams()
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                return ctx.Origin_Exam.ToList();
            }
        }
        public List<Origin_Exam> GetOriginExamsByExamId(int id)
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                var exams = ctx.Origin_Exam.Where(e => e.ExamId == id).ToList();
                //System.Diagnostics.Trace.WriteLine(" --i-- GetOriginExamsByExamId-id: " + id);
                //foreach ( Origin_Exam exam in exams) {
                //    System.Diagnostics.Trace.WriteLine(" --i--  GetOriginExamsByExamId-exam.ExamId: " + exam.ExamId);
                //    System.Diagnostics.Trace.WriteLine(" --i--  GetOriginExamsByExamId-exam.OriginId: " + exam.OriginId);
                //}
                return exams;
            }
        }
    }
}