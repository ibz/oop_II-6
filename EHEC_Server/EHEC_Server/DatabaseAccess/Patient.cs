﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace EHEC_Server
{
    public partial class Patient
    {
        public Patient CreatePatient(Patient patient)
        {
            try
            {
                using (EHEC_DBEntities ctx = new EHEC_DBEntities())
                {
                    patient.PatientUid = Guid.NewGuid().ToString();
                    ctx.Patients.Add(patient);
                    ctx.SaveChanges();
                }
                return patient;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(" --e-- Error in ServerCreatePatient: " + e);
                return new Patient();
            }
        }

        public List<Patient> GetAllPatients()
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                return ctx.Patients.ToList();
            }
        }
        public Patient GetPatientIdById(int id)
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                Patient patient = ctx.Patients.Where(e => e.PatientId == id).SingleOrDefault();
                return patient;
            }
        }

    }
}