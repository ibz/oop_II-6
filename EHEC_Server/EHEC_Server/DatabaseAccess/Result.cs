﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace EHEC_Server
{
    public partial class Result
    {
        public Result CreateResult(Result result)
        {
            Result r = new Result();
            List<Result> results = new List<Result>();
            results = r.GetAllResults();
            foreach(Result element in results)
            {
                if (element.Name == result.Name)
                {
                    result.ResultId = element.ResultId;
                    return result;
                }
            }
            try
            {
                using (EHEC_DBEntities ctx = new EHEC_DBEntities())
                {
                    result.ResultUid = Guid.NewGuid().ToString();
                    ctx.Results.Add(result);
                    ctx.SaveChanges();
                }
                return result;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(" --e-- Error in ServerCreateResult: " + e);
                return new Result();
            }
        }
        public List<Result> GetAllResults()
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                ctx.Configuration.ProxyCreationEnabled = false;
                return ctx.Results.ToList();
            }
        }
        public Result GetResultsById(int id)
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                Result result = ctx.Results.Where(e => e.ResultId == id).SingleOrDefault();
                return result;
            }
        }
    }
}