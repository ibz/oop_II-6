﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace EHEC_Server
{
    public partial class Origin
    {
        public Origin CreateOrigin(Origin origin)
        {
            
            try
            {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                origin.OriginUid = Guid.NewGuid().ToString();
                ctx.Origins.Add(origin);
                ctx.SaveChanges();
            }
            return origin;
                }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(" --e-- Error in ServerCreateOrigin: " + e);
                return new Origin();
            }
            
        }
        public List<Origin> GetAllOrigins()
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                return ctx.Origins.ToList();
            }
        }
        public Origin GetOriginByOriginId(int id)
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                Origin origin = ctx.Origins.Where(o => o.OriginId == id).SingleOrDefault();
                return origin;
            }
        }

    }
}