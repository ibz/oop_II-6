﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;


namespace EHEC_Server
{
    public partial class Exam
    {
        public Exam CreateExam(Exam exam)
        {
            try
            {
                using (EHEC_DBEntities ctx = new EHEC_DBEntities())
                {
                    exam.ExamUid = Guid.NewGuid().ToString();
                    ctx.Exams.Add(exam);
                    ctx.SaveChanges();
                }
                return exam;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(" --e-- Error in ServerCreateExam: " + e);
                return new Exam();
            }
        }
        public List<Exam> GetAllExams()
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                return ctx.Exams.ToList();
            }
        }
        public List<Exam> GetExamsByPatientId(int patientId)
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                List<Exam> exams = ctx.Exams.Where(e => e.PatientId == patientId).ToList();
                return exams;
            }
        }
    }
}