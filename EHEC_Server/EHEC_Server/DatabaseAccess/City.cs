﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EHEC_Server
{
    public partial class City
    {
        public City CreateCity(City city)
        {
            City c = new City();
            List<City> cities = new List<City>();
            cities = c.GetAllCities();
            foreach (City element in cities)
            {
                if (element.CityName == city.CityName)
                {
                    city.CityId = element.CityId;
                    return city;
                }
            }
            try
            {
                using (EHEC_DBEntities ctx = new EHEC_DBEntities())
                {
                    city.CityUid = Guid.NewGuid().ToString();
                    ctx.Cities.Add(city);
                    ctx.SaveChanges();
                }
                return city;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(" --e-- Error in ServerCreateCity: " + e);
                return new City();
            }
        }
        public List<City> GetAllCities()
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                return ctx.Cities.ToList();
            }
        }
        public City GetCityByCityId(int id)
        {
            using (EHEC_DBEntities ctx = new EHEC_DBEntities())
            {
                City City = ctx.Cities.Where(c => c.CityId == id).FirstOrDefault();
                return City;
            }
        }
    }
}