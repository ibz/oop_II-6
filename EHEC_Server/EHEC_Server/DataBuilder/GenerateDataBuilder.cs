﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace EHEC_Server
{
    public partial class GenerateDataBuilder
    {

        //dieses file wird im moment nicht benötigt, da alles im global benutzt wird, bitte so belassen für den moment
        private static List<Doctor> Doctors = new List<Doctor>();
        private static List<Patient> Patients = new List<Patient>();
        private static List<Origin> Origins = new List<Origin>();
        private static List<Result> Results = new List<Result>();
        private static List<Exam> Exams = new List<Exam>();
        private static Random Rnd = new Random();


        private static List<string> FirstNames = new List<string>(new string[]
        {
            "Michael", "Andreas", "Stefan", "Ivan","Adrien"
        });

        private static List<string> LastNames = new List<string>(new string[]
        {
            "Meister", "Schär", "Eberhard", "Zingg", "Howald", "Aebi", "Feldmann"
        });

        private static List<string> Streets = new List<string>(new string[]
        {
            "Rosenauweg", "Schessstrasse", "Hardstrasse", "Gehweg", "Bananastreet", "Moosweg", "Feldstrasse"
        });

        private static List<string> Cities = new List<string>(new string[]
        {
            "Aarau", "Bern", "Zürich", "Winterthur", "Lausanne", "Luzern", "Zug", "Fribourg", 
            "Chur", "Baden", "Burgdorf", "Will", "Romanshorn", "Düdingen", "Heitenried", "Wiler vor Holz",
            "Brünnisried",
        });

        private static List<string> Regions = new List<string>(new string[]
        {
            "Aargau", "Zurich", "Graubünden","Vaudoise", "Mittelland", "Westschweiz"
        });


        private static List<string> BirthDates = new List<string>(new string[]
        {
            "2001-07-11 20:18:00.000", "1999-03-12 10:18:00.000","1980-01-21 00:18:00.000",
        });

        private static List<string> DoctorOrigins = new List<string>(new string[]
        {
            "Mittelland", "Südostschweiz", "Westschweiz", "Bündnerland"
        });

        private static List<string> Foods = new List<string>(new string[]
        {
            "Gurke", "Salat", "Rüebli", "Sprossen", "Zuchini", "Tomaten"
        });

        private static List<string> Bacterias = new List<string>(new string[]
        {
            "EHEC-A", "EHEC-B", "EHEC-C", "EHEC-D", "EHEC-E", "EHEC-F"
        });

        private static List<string> RestaurantNames = new List<string>(new string[]
        {
            "zur Linde", "Schützen", "Goldener Apfel", "Kafi Meier", "Mürset"
        });

        public static bool CreateRandomData(int valueToCreate)
        {
            try
            {

                Random r = new Random();
                Random r1 = new Random(5);
                Random r2 = new Random(41);
                Random r3 = new Random(137);
                Random r4 = new Random(5327);
                for (int i = 0; i < valueToCreate; i++)
                {
                    Patient patient = new Patient
                    {
                        FirstName = FirstNames[r.Next(0, FirstNames.Count())].ToString(),
                        LastName = LastNames[r.Next(0, LastNames.Count())].ToString(),
                        BirthDate = Convert.ToDateTime(BirthDates[r.Next(0, BirthDates.Count())].ToString()),
                        Street = Streets[r.Next(0, Streets.Count())].ToString(),
                        City = Cities[r.Next(0, Cities.Count())].ToString(),
                        Region = Regions[r.Next(0, Regions.Count())].ToString()
                    };
                    patient.CreatePatient(patient);

                    Doctor doctor = new Doctor
                    {
                        FirstName = FirstNames[r.Next(0, FirstNames.Count())].ToString(),
                        LastName = LastNames[r.Next(0, LastNames.Count())].ToString(),
                        DoctorOrigin = DoctorOrigins[r.Next(0, DoctorOrigins.Count())].ToString(),
                        Region = Regions[r.Next(0, Regions.Count())].ToString()
                    };
                    doctor.CreateDoctor(doctor);

                    Result result = new Result
                    {
                        Name = Bacterias[r.Next(0, Bacterias.Count())].ToString(),
                    };
                    result.CreateResult(result);

                    Exam exam = new Exam
                    {
                        DoctorId = doctor.DoctorId,
                        PatientId = patient.PatientId,
                        ResultId = result.ResultId
                    };
                    exam.CreateExam(exam);

                    City city1 = new City
                    {
                        CityName = Cities[r1.Next(0, Cities.Count())].ToString()
                    };
                    city1.CreateCity(city1);
                    City city2 = new City
                    {
                        CityName = Cities[r1.Next(0, Cities.Count())].ToString()
                    };
                    city2.CreateCity(city2);
                    City city3 = new City
                    {
                        CityName = Cities[r1.Next(0, Cities.Count())].ToString()
                    };
                    city3.CreateCity(city3);
                    City city4 = new City
                    {
                        CityName = Cities[r1.Next(0, Cities.Count())].ToString()
                    };
                    city4.CreateCity(city4);
                    Origin origin1 = new Origin
                    {
                        Name = RestaurantNames[r1.Next(0, RestaurantNames.Count())].ToString(),
                        Food = Foods[r1.Next(0, Foods.Count())].ToString(),
                        Street = Streets[r1.Next(0, Streets.Count())].ToString(),
                        CityId = city1.CityId
                    };
                    origin1.CreateOrigin(origin1);
                    Origin origin2 = new Origin
                    {
                        Name = RestaurantNames[r2.Next(0, RestaurantNames.Count())].ToString(),
                        Food = Foods[r2.Next(0, Foods.Count())].ToString(),
                        Street = Streets[r2.Next(0, Streets.Count())].ToString(),
                        CityId = city2.CityId
                    };
                    origin2.CreateOrigin(origin2);
                    Origin origin3 = new Origin
                    {
                        Name = RestaurantNames[r3.Next(0, RestaurantNames.Count())].ToString(),
                        Food = Foods[r3.Next(0, Foods.Count())].ToString(),
                        Street = Streets[r3.Next(0, Streets.Count())].ToString(),
                        CityId = city3.CityId
                    };
                    origin3.CreateOrigin(origin3);
                    Origin origin4 = new Origin
                    {
                        Name = RestaurantNames[r4.Next(0, RestaurantNames.Count())].ToString(),
                        Food = Foods[r4.Next(0, Foods.Count())].ToString(),
                        Street = Streets[r4.Next(0, Streets.Count())].ToString(),
                        CityId = city4.CityId
                    };
                    origin4.CreateOrigin(origin4);

                    Origin_Exam origin_exam1 = new Origin_Exam
                    {
                        ExamId = exam.ExamId,
                        OriginId = origin1.OriginId
                    };
                    origin_exam1.CreateOrigin_Exam(origin_exam1);
                    Origin_Exam origin_exam2 = new Origin_Exam
                    {
                        ExamId = exam.ExamId,
                        OriginId = origin2.OriginId
                    };
                    origin_exam2.CreateOrigin_Exam(origin_exam2);
                    Origin_Exam origin_exam3 = new Origin_Exam
                    {
                        ExamId = exam.ExamId,
                        OriginId = origin3.OriginId
                    };
                    origin_exam3.CreateOrigin_Exam(origin_exam3);
                    Origin_Exam origin_exam4 = new Origin_Exam
                    {
                        ExamId = exam.ExamId,
                        OriginId = origin4.OriginId
                    };
                    origin_exam4.CreateOrigin_Exam(origin_exam4);
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(" --e-- Error in CreateRandomData: " + e);
                return false;
            }

        }
    }
}