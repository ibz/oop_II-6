﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;
using Client.EHEC_Service;
using AutoMapper;


namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            // Start Automapper
            Mapper.Initialize(cfg => cfg.AddProfile<AutomapperProfile>());
            try
            {   // loading of dropdowns
                GetResults();
                // check if Server(ping) answers throug a invoke of ButtonOnline_Click
                CheckOnline();
            }
            catch (Exception ex)
            {
                LabelRueckmeldungsfeld.Content = "Bei der Initialisierung ist ein Fehler passiert.";
                System.Diagnostics.Trace.WriteLine(" --e-- Error in Client MainWindow init: " + ex);
            }


        }
        public void GetResults()
        {
            ComboBoxBakterienstamm.ItemsSource = Global.GlobalInstance.Service.GetResults();
            ComboBoxBakterienstamm.DisplayMemberPath = "Name";
        }
        public void CheckOnline()
        {
            ButtonOnline.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }
        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void ButtonSend_Click(object sender, RoutedEventArgs e)
        {
            LabelRueckmeldungsfeld.Content = "Sende Daten... ";
            // wenn die Anzahl im Feld "mehrere Automatisch generieren"
            // grösser ist als 0 dann erstelle diese Anzahl automatisch.
            // hierzu wird nur die Anzahl an den Server übertragen und 
            // der Server generiert die Zufallsdatensätze.
            DateTime abgabedatum = new DateTime(2018, 9, 15);
            string anzahltext = TextboxCreateManyAnzahl.Text.ToString();
            bool anzahlbool = int.TryParse(anzahltext, out int anzahl);
            if (anzahlbool)
            {
                if (anzahl < 9999)
                {
                    LabelRueckmeldungsfeld.Content = "Es werden " + anzahl + " Datensätze erstellt...";
                    bool feedback = false;
                    try
                    {
                        feedback = Global.GlobalInstance.Service.CreateRandomData(Convert.ToInt32(TextboxCreateManyAnzahl.Text));
                    }
                    catch (Exception ex)
                    {
                        LabelRueckmeldungsfeld.Content = "Fehler in der erzeugung von " + TextboxCreateManyAnzahl.Text + " neuen Einträgen.";
                        System.Diagnostics.Trace.WriteLine(" --e-- Error in Client CreateRandomData: " + ex);
                    }

                    if (feedback == true)
                    {
                        LabelRueckmeldungsfeld.Content = "Fertig. " + TextboxCreateManyAnzahl.Text + " neue Einträge gespeichert.";
                    }
                    if (feedback == false)
                    {
                        LabelRueckmeldungsfeld.Content = "Fehler in der erzeugung von " + TextboxCreateManyAnzahl.Text + " neuen Einträgen.";
                    }
                }
                if (anzahl > 9998 && DateTime.UtcNow < abgabedatum)
                {
                    LabelRueckmeldungsfeld.Content = "nei Strathi, me mues me nid " + anzahl + " Datesätz erstelle... ;-) ";
                }
                if (anzahl > 9998 && DateTime.UtcNow >= abgabedatum)
                {
                    LabelRueckmeldungsfeld.Content = "Das Erstellen von mehr als 9998 Testdatensätze ist nicht erlaubt. ";
                }
            }
            // wenn das Feld "mehrere Automatisch generieren" kleiner ist als 0 dann 
            // erstelle einzelnen Eintrag auf dem Client.
            if (anzahl == 0)
            {
                try
                {
                    // 1. Doctor
                    Doctor newDoctor = new Doctor(TextboxArztVorname.Text,
                                                  TextboxArztName.Text,
                                                  TextboxArztStrasseNr.Text,
                                                  TextboxArztPlz.Text,
                                                  TextboxArztOrt.Text,
                                                  TextboxArztRegion.Text,
                                                  TextboxArztKantonsarzt.Text
                                                  );

                    // 2. Patient
                    Patient newPatient = new Patient(TextboxPatientVorname.Text,
                                                     TextboxPatientName.Text,
                                                     TextboxPatientStrasseNr.Text,
                                                     TextboxPatientPlz.Text,
                                                     TextboxPatientOrt.Text,
                                                     TextboxPatientRegion.Text,
                                                     TextboxPatientGeburtstag.Text
                                                     );

                    // 3. Result
                    Result newResult = new Result(ComboBoxBakterienstamm.Text);

                    LabelRueckmeldungsfeld.Content += "erstelle Doktor, ";
                    newDoctor.CreateDoctor();
                    LabelArztId.Content = Convert.ToString(newDoctor.Id);

                    LabelRueckmeldungsfeld.Content += "erstelle Patient, ";
                    newPatient.CreatePatient();
                    LabelPatientId.Content = Convert.ToString(newPatient.Id);

                    LabelRueckmeldungsfeld.Content += "erstelle Bakterie, ";
                    newResult.CreateResult();
                    LabelBakterienstamm.Content = Convert.ToString(newResult.Id);
                    // 4. Exam
                    Exam newExam = new Exam(newDoctor.Id,
                                            newPatient.Id,
                                            newResult.Id
                                            );
                    LabelRueckmeldungsfeld.Content += "erstelle Untersuchung, ";
                    newExam.CreateExam();
                    City city1 = new City(TextboxNahrung1Ort.Text);
                    city1.CreateCity();
                    City city2 = new City(TextboxNahrung2Ort.Text);
                    city2.CreateCity();
                    City city3 = new City(TextboxNahrung3Ort.Text);
                    city3.CreateCity();
                    City city4 = new City(TextboxNahrung4Ort.Text);
                    city4.CreateCity();
                    // 5. Origin
                    Origin newOrigin1 = new Origin(TextboxNahrung1Restaurant.Text,
                                                   TextboxNahrung1Essen.Text,
                                                   TextboxNahrung1Strasse.Text,
                                                   Convert.ToInt32(TextboxNahrung1Plz.Text),
                                                   city1.Id
                                                   );

                    Origin newOrigin2 = new Origin(TextboxNahrung2Restaurant.Text,
                                                   TextboxNahrung2Essen.Text,
                                                   TextboxNahrung2Strasse.Text,
                                                   Convert.ToInt32(TextboxNahrung2Plz.Text),
                                                   city2.Id
                                                   );

                    Origin newOrigin3 = new Origin(TextboxNahrung3Restaurant.Text,
                                                   TextboxNahrung3Essen.Text,
                                                   TextboxNahrung3Strasse.Text,
                                                   Convert.ToInt32(TextboxNahrung3Plz.Text),
                                                   city3.Id
                                                   );

                    Origin newOrigin4 = new Origin(TextboxNahrung4Restaurant.Text,
                                                   TextboxNahrung4Essen.Text,
                                                   TextboxNahrung4Strasse.Text,
                                                   Convert.ToInt32(TextboxNahrung4Plz.Text),
                                                   city4.Id
                                                   );

                    LabelRueckmeldungsfeld.Content += "erstelle Nahrungsaufnahmen.";

                    newOrigin1.CreateOrigin();
                    LabelNahrung1Id.Content = Convert.ToString(newOrigin1.Id);

                    newOrigin2.CreateOrigin();
                    LabelNahrung2Id.Content = Convert.ToString(newOrigin2.Id);

                    newOrigin3.CreateOrigin();
                    LabelNahrung3Id.Content = Convert.ToString(newOrigin3.Id);

                    newOrigin4.CreateOrigin();
                    LabelNahrung4Id.Content = Convert.ToString(newOrigin4.Id);
                    // 6. Origin_Exam
                    Origin_Exam newOrigin_Exam1 = new Origin_Exam(newOrigin1.Id,
                                                                 newExam.Id
                                                                 );

                    Origin_Exam newOrigin_Exam2 = new Origin_Exam(newOrigin2.Id,
                                                                 newExam.Id
                                                                 );

                    Origin_Exam newOrigin_Exam3 = new Origin_Exam(newOrigin3.Id,
                                                                 newExam.Id
                                                                 );

                    Origin_Exam newOrigin_Exam4 = new Origin_Exam(newOrigin4.Id,
                                                                 newExam.Id
                                                                 );

                    LabelRueckmeldungsfeld.Content += "Verlinke Nahrungsaufnahmen mit der Untersuchungsnummer.";
                    newOrigin_Exam1.CreateOrigin_Exam();
                    newOrigin_Exam2.CreateOrigin_Exam();
                    newOrigin_Exam3.CreateOrigin_Exam();
                    newOrigin_Exam4.CreateOrigin_Exam();
                    LabelRueckmeldungsfeld.Content = "Fertig. Behandlung Nr." + Convert.ToString(newExam.Id) + " gespeichert.";
                    CleanExam();
                    Global.GlobalInstance.Service.ReloadCluster();
                }
                catch (Exception ex)
                {
                    LabelRueckmeldungsfeld.Content = "Fehler in der erzeugung dieser Behandlung";
                    System.Diagnostics.Trace.WriteLine(" --e-- Error in Client Save: " + ex);
                }
            }
            GetResults();
            
        }
        private void CleanExam()
        {
            try
            {
                TextboxPatientName.Clear();
                TextboxPatientVorname.Clear();
                TextboxPatientStrasseNr.Clear();
                TextboxPatientPlz.Clear();
                TextboxPatientOrt.Clear();
                TextboxPatientRegion.Clear();
                TextboxPatientGeburtstag.Clear();
                ComboBoxBakterienstamm.SelectedItem = null; //ItemsControl.ItemsSource
                TextboxNahrung1Restaurant.Clear();
                TextboxNahrung1Essen.Clear();
                TextboxNahrung1Strasse.Clear();
                TextboxNahrung1Plz.Clear();
                TextboxNahrung1Ort.Clear();
                TextboxNahrung2Restaurant.Clear();
                TextboxNahrung2Essen.Clear();
                TextboxNahrung2Strasse.Clear();
                TextboxNahrung2Plz.Clear();
                TextboxNahrung2Ort.Clear();
                TextboxNahrung3Restaurant.Clear();
                TextboxNahrung3Essen.Clear();
                TextboxNahrung3Strasse.Clear();
                TextboxNahrung3Plz.Clear();
                TextboxNahrung3Ort.Clear();
                TextboxNahrung4Restaurant.Clear();
                TextboxNahrung4Essen.Clear();
                TextboxNahrung4Strasse.Clear();
                TextboxNahrung4Plz.Clear();
                TextboxNahrung4Ort.Clear();
            }
            catch (Exception ex)
            {
                LabelRueckmeldungsfeld.Content = "Fehler in der löschung der felder";
                System.Diagnostics.Trace.WriteLine(" --e-- Error in Client CleanExam: " + ex);
            }
        }
        private void ButtonAutogenerateOne_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TextboxArztName.Text = new Bogus.DataSets.Name("de_CH").LastName();
                TextboxArztVorname.Text = new Bogus.DataSets.Name("de_CH").FirstName();
                TextboxArztStrasseNr.Text = new Bogus.DataSets.Address("de_CH").StreetName();
                TextboxArztPlz.Text = new Bogus.DataSets.Address("de_CH").ZipCode();
                TextboxArztOrt.Text = new Bogus.DataSets.Address("de_CH").City();
                TextboxArztKantonsarzt.Text = Testdata.RandomEnumValue<Testdata.Doctortyp>().ToString();
                TextboxArztRegion.Text = new Bogus.DataSets.Address("de_CH").State();
                TextboxPatientName.Text = new Bogus.DataSets.Name("de_CH").LastName();
                TextboxPatientVorname.Text = new Bogus.DataSets.Name("de_CH").FirstName();
                TextboxPatientStrasseNr.Text = new Bogus.DataSets.Address("de_CH").StreetName();
                TextboxPatientPlz.Text = new Bogus.DataSets.Address("de_CH").ZipCode();
                TextboxPatientOrt.Text = new Bogus.DataSets.Address("de_CH").City();
                TextboxPatientRegion.Text = new Bogus.DataSets.Address("de_CH").State();
                TextboxPatientGeburtstag.Text = new Bogus.DataSets.Date().Past().ToString();
                ComboBoxBakterienstamm.Text = "bacteriaName";
                TextboxNahrung1Restaurant.Text = Testdata.RandomEnumValue<Testdata.RestaurantPre>().ToString() +
                                          " " + Testdata.RandomEnumValue<Testdata.RestaurantName>().ToString();
                TextboxNahrung1Essen.Text = Testdata.RandomEnumValue<Testdata.Vegie>().ToString() +
                                            " " + Testdata.RandomEnumValue<Testdata.Vegie>().ToString();
                TextboxNahrung1Strasse.Text = new Bogus.DataSets.Address("de_CH").StreetName();
                TextboxNahrung1Plz.Text = new Bogus.DataSets.Address("de_CH").ZipCode();
                TextboxNahrung1Ort.Text = new Bogus.DataSets.Address("de_CH").City();
                TextboxNahrung2Restaurant.Text = Testdata.RandomEnumValue<Testdata.RestaurantPre>().ToString() +
                                          " " + Testdata.RandomEnumValue<Testdata.RestaurantName>().ToString();
                TextboxNahrung2Essen.Text = Testdata.RandomEnumValue<Testdata.Vegie>().ToString() +
                                            " " + Testdata.RandomEnumValue<Testdata.Vegie>().ToString();
                TextboxNahrung1Strasse.Text = new Bogus.DataSets.Address("de_CH").StreetName();
                TextboxNahrung2Plz.Text = new Bogus.DataSets.Address("de_CH").ZipCode();
                TextboxNahrung2Ort.Text = new Bogus.DataSets.Address("de_CH").City();
                TextboxNahrung3Restaurant.Text = Testdata.RandomEnumValue<Testdata.RestaurantPre>().ToString() +
                                            " " + Testdata.RandomEnumValue<Testdata.RestaurantName>().ToString();
                TextboxNahrung3Essen.Text = Testdata.RandomEnumValue<Testdata.Vegie>().ToString() +
                                            " " + Testdata.RandomEnumValue<Testdata.Vegie>().ToString();
                TextboxNahrung3Strasse.Text = new Bogus.DataSets.Address("de_CH").StreetName();
                TextboxNahrung3Plz.Text = new Bogus.DataSets.Address("de_CH").ZipCode();
                TextboxNahrung3Ort.Text = new Bogus.DataSets.Address("de_CH").City();
                TextboxNahrung4Restaurant.Text = Testdata.RandomEnumValue<Testdata.RestaurantPre>().ToString() +
                                            " " + Testdata.RandomEnumValue<Testdata.RestaurantName>().ToString();
                TextboxNahrung4Essen.Text = Testdata.RandomEnumValue<Testdata.Vegie>().ToString() +
                                            " " + Testdata.RandomEnumValue<Testdata.Vegie>().ToString();
                TextboxNahrung4Strasse.Text = new Bogus.DataSets.Address("de_CH").StreetName();
                TextboxNahrung4Plz.Text = new Bogus.DataSets.Address("de_CH").ZipCode();
                TextboxNahrung4Ort.Text = new Bogus.DataSets.Address("de_CH").City();
            }
            catch (Exception ex)
            {
                LabelRueckmeldungsfeld.Content = "Fehler in der löschung der felder";
                System.Diagnostics.Trace.WriteLine(" --e-- Error in Client Auto One: " + ex);
            }
        }
        private void ButtonOnline_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string con = Global.GlobalInstance.Service.Ping();
                if (con == "pong")
                {
                    ButtonOnline.Content = "online";
                    ButtonOnline.Foreground = Brushes.Green;
                }
            }
            catch (Exception)
            {
                ButtonOnline.Content = "offline";
                ButtonOnline.Foreground = Brushes.Red;
            }
        }
    }
}
