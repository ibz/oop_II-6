﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;

namespace Client
{
    
    class Testdata
    {
        public static T RandomEnumValue<T>()
        {
            var v = Enum.GetValues(typeof(T));
            return (T)v.GetValue(new Random(DateTime.Now.Millisecond).Next(v.Length));
        }
        public enum Doctortyp
        {
            Kantonsarzt,
            Regionalarzt,
            BAGArzt
        }
        public enum RestaurantPre
        {
            None,
            Zum,
            Auf,
            Im,
            Goldenen,
            Silbrigen,
            Panorama,
            Warmen,
            Schönen,
        }
        public enum RestaurantName
        {
            Schützen,
            Hirschen,
            Ochsen,
            Frohsinn,
            Hüftgold,
            Seerose,
            Rosengarten,
            Schäferstube,
            Tramdepot,
            Braui,
            Taverne,
            Schiff,
            Bellavista,
            Quellenhof,
            Volkshaus,
            Panorama
        }
        public enum Vegie
        {
            Gurken,
            Salat,
            Rüben,
            Karrotten,
            Spinat,
            Okra,
            Bohnen,
            Sprossen,
            Tomaten,
            Chabis,
            Kartoffeln,
            Spargeln,
            Pilze,
            Radieschen,
            Mais
        }
        //var doctor = new Faker<Doctor>()
        //    .RuleFor(d => d.FirstName, f => f.Name.FirstName())
        //    .RuleFor(d => d.LastName, f => f.Name.LastName())
        //    .RuleFor(d => d.Strasse, f => f.Address.StreetName())
        //    .RuleFor(d => d.Plz, f => f.Address.ZipCode())
        //    .RuleFor(d => d.Ort, f => f.Address.City())
        //    .RuleFor(d => d.Region, f => f.Address.State())
        //    .RuleFor(d => d.DoctorOrigin, f => f.PickRandom<Doctortyp>())
        //    ;
        //.RuleFor(d => d.
    }
}
