﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Client
{
    public class Doctor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Strasse { get; set; }
        public string Plz { get; set; }
        public string Ort { get; set; }
        public string Region { get; set; }
        public string DoctorOrigin { get; set; }
        public Doctor() {}
        /// <summary>
        /// this is my doctor construcor
        /// </summary>
        /// <param name="firstName"> first name of the doctor</param>
        /// <param name="lastName">last name of the doctor</param>
        /// <param name="region">region where the doctor works</param>
        /// <param name="doctype">Kantonal, Regional or Local Doctor</param>
        public Doctor(string firstName, 
                      string lastName,
                      string strasse,
                      string plz, 
                      string ort, 
                      string region, 
                      string doctype 
            )
        {
            FirstName = firstName;
            LastName = lastName;
            Strasse = strasse;
            Plz = plz;
            Ort = ort;
            Region = region;
            DoctorOrigin = doctype;
        }

        /// <summary>
        /// This writes the doctor back to the service into the db
        /// </summary>
        public Doctor CreateDoctor()
        {
            // write doctor to wcf interface
            EHEC_Service.Doctor mydoctor = new EHEC_Service.Doctor
            {
                FirstName = FirstName,
                LastName = LastName,
                Region = Region,
                DoctorOrigin = DoctorOrigin
            };
            Id = Global.GlobalInstance.Service.WriteDoctor(mydoctor).DoctorId;
            return Mapper.Map<Doctor>(mydoctor);
        }

    }
}
