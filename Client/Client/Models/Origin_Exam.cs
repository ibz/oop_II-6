﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Client
{
    public class Origin_Exam
    {
        public int Id { get; set; }
        public int OriginOriginId { get; set; }
        public int ExamExamId { get; set; }

        public Origin_Exam()
        {

        }
        public Origin_Exam(int originOriginId,
                           int examExamId
                           )
        {
            OriginOriginId = originOriginId;
            ExamExamId = examExamId;
        }
        public Origin_Exam CreateOrigin_Exam()
        {
            EHEC_Service.Origin_Exam myorigin_Exam = new EHEC_Service.Origin_Exam
            {
                OriginId = OriginOriginId,
                ExamId = ExamExamId
            };
            Id = Global.GlobalInstance.Service.WriteOrigin_Exam(myorigin_Exam).Origin_ExamId;
            return Mapper.Map<Origin_Exam>(myorigin_Exam);
        }
    }
}
