﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class City
    {
        public int Id { get; set; }
        public string CityName { get; set; }

        public City(string cityName
                    )
        {
            CityName = cityName;
        }
        public City() { }
        public City CreateCity()
        {
            EHEC_Service.City myCity = new EHEC_Service.City
            {
                CityName = CityName,
            };

            ////Mapper back to Object of Local 'City' object but with
            ////the new 'EHEC_Service.City' in the db created id:
            //City c = new City()
            //{
            //    CityName = myCity.CityName,
            //};
            //return c;

            // Automapper version:
            Id = Global.GlobalInstance.Service.WriteCity(myCity).CityId;
            return AutoMapper.Mapper.Map<City>(myCity);
        }
    }
}
