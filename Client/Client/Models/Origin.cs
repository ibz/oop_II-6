﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Client
{
    public class Origin
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Food { get; set; }
        public string Street { get; set; }
        public int Plz { get; set; }
        public int CityId { get; set; }

        public Origin()
        {

        }
        public Origin(string restName,
                      string foodtype,
                      string street,
                      int plz,
                      int cityId
            )
        {
            Name = restName;
            Food = foodtype;
            Street = street;
            Plz = plz;
            CityId = cityId;
        }

        public Origin CreateOrigin()
        {
            EHEC_Service.Origin myorigin = new EHEC_Service.Origin
            {
                Name = Name,
                Food = Food,
                Street = Street,
                CityId = CityId,
            };
            Id = Global.GlobalInstance.Service.WriteOrigin(myorigin).OriginId;
            return Mapper.Map<Origin>(myorigin);
        }
    }
}
