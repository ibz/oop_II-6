﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Client
{
    public class Exam
    {
        public int Id { get; set; }
        public int DoctorID { get; set; }
        public int PatientID { get; set; }
        public int ResultID { get; set; }
        public Exam() { }
        public Exam(int doctorId,
                    int patientId,
                    int resultId
                    ){
            DoctorID = doctorId;
            PatientID = patientId;
            ResultID = resultId;
        }
        public Exam CreateExam()
        {
            EHEC_Service.Exam myExam = new EHEC_Service.Exam
            {
                DoctorId = DoctorID,
                PatientId = PatientID,
                ResultId = ResultID
            };
            Id = Global.GlobalInstance.Service.WriteExam(myExam).ExamId;
            return Mapper.Map<Exam>(myExam);
        }
    }
}
