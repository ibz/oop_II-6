﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Client
{
    class AutomapperProfile: Profile
    {
        public AutomapperProfile()
        {
            CreateMap<EHEC_Service.Patient, Patient>();
            CreateMap<Patient, EHEC_Service.Patient>();
            CreateMap<EHEC_Service.Doctor, Doctor>();
            CreateMap<Doctor, EHEC_Service.Doctor>();
            CreateMap<EHEC_Service.Exam, Exam>();
            CreateMap<Exam, EHEC_Service.Exam>();
            CreateMap<EHEC_Service.Origin, Origin>();
            CreateMap<Origin, EHEC_Service.Origin>();
            CreateMap<EHEC_Service.Origin_Exam, Origin_Exam>();
            CreateMap<Origin_Exam, EHEC_Service.Origin_Exam>();
            CreateMap<EHEC_Service.Result,Result>();
            CreateMap<Result, EHEC_Service.Result>();
            CreateMap<EHEC_Service.City, City>();
            CreateMap<City, EHEC_Service.City>();
        }
    }
}
