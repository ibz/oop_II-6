﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Client
{
    public class Result
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Result()
        {

        }

        public Result(string resultName)
        {
            Name = resultName;
        }
        public Result CreateResult()
        {
            EHEC_Service.Result myResult = new EHEC_Service.Result
            {
                Name = Name
            };
            Id = Global.GlobalInstance.Service.WriteResult(myResult).ResultId;
            return Mapper.Map<Result>(myResult);
            }
    }
}
