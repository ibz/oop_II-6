﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
namespace Client
{
    public class Patient
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street { get; set; }
        public int Plz { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string BirthDate { get; set; }

        public Patient(string firstName, 
                       string lastName, 
                       string street,
                       string plz,
                       string city,
                       string region,
                       string birthDate
                       )
        {
            FirstName = firstName;
            LastName = lastName;
            Street = street;
            Plz = Convert.ToInt32(plz);
            City = city;
            Region = region;
            BirthDate = birthDate;
        }
        public Patient() { }
        public Patient CreatePatient()
        {
            EHEC_Service.Patient mypatient = new EHEC_Service.Patient
            {
                FirstName = FirstName,
                LastName = LastName,
                Street = Street,
                City = City,
                Region = Region,
                BirthDate = Convert.ToDateTime(BirthDate)
            };

            ////Mapper back to Object of Local 'Patient' object but with
            ////the new 'EHEC_Service.Patient' in the db created id:
            //Patient p = new Patient()
            //{
            //    Id = mypatient.PatientId,
            //    FirstName = mypatient.FirstName,
            //    LastName = mypatient.LastName,
            //    Street = mypatient.Street,
            //    City = mypatient.City,
            //    Region = mypatient.Region,
            //    BirthDate = Convert.ToString(mypatient.BirthDate)
            //};
            //return p;

            // Automapper version:
            Id = Global.GlobalInstance.Service.WritePatient(mypatient).PatientId;
            return Mapper.Map<Patient>(mypatient);
        }
    }
}
