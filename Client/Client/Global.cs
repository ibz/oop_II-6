﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Client.EHEC_Service;
using System.Windows;

namespace Client
{
    /// <summary>
    /// This is a Singleton implementation for the need of one single client connection
    /// </summary>

    public class Global
    {
        private Global()
        {

        }

        private static Global globalInstance = null;
        private static ServiceClient service = null;

        public static Global GlobalInstance {
            get {
                if (globalInstance == null)
                {
                    globalInstance = new Global();
                }
                return globalInstance;
            }
        }

        public ServiceClient Service
        {
            get {
                if (service == null)
                {
                    service = new ServiceClient("BasicHttpBinding_IService");
                }
                return service;
            }
        }

    }
}
