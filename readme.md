This is a C# project.
To compile do this:
1. open EHEC_Server/EHEC_Server.sln in Visual Studio 2017
2. Rightklick on solution and do a 'Restore Nuget Packages'.
3. Save, close and re-open the Solution.
4. if your SQL DB is not in root-folder:
   -navigate to web.config and search for connectionString.
   -change source=.\SQLEXPRESS to your path.
5. -navigate to folder SQL and create the database by executing
    the db_create.sql file on your DB connection.
6. press start.
7. it should now start 1 WPF Form and load your Browser with the cluster.
