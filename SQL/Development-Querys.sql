/****** Skript for SelectTopNRows - for development purposes ******/

SELECT TOP (100) *
FROM [EHEC_DB].[dbo].[Patient]

SELECT TOP (100) *
FROM [EHEC_DB].[dbo].[Doctor]

SELECT TOP (100) *
FROM [EHEC_DB].[dbo].[Exam]

SELECT TOP (100) *
FROM [EHEC_DB].[dbo].[Result]

SELECT TOP (100) *
FROM [EHEC_DB].[dbo].[Origin_Exam]

SELECT TOP (100) * 
  FROM [EHEC_DB].[dbo].[Origin]

SELECT TOP (100) * FROM Exam
JOIN Patient on Exam.PatientId = Patient.PatientId
JOIN Doctor on Exam.DoctorId = Doctor.DoctorId
JOIN Result on Exam.ResultId = Result.ResultId
JOIN Origin_Exam on Exam.ExamId = Origin_Exam.ExamId
JOIN Origin on Origin_Exam.OriginId = Origin.OriginId 
JOIN City on Origin.CityId = City.CityId

