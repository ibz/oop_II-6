
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/09/2018 13:24:19
-- Generated from Cadaroski Ismail
-----------------------------------------------------
-- CHANGE LOG:
-- 180618 Ivan, Add Create DB
-- 180621 Ismail, change Tables
-- 180629 Ivan, Add inserts for fixed list
-- --------------------------------------------------

-- Auskommentieren f�r L�schung der Datenbank
IF db_id('EHEC_DB') is not null
  BEGIN;
	PRINT 'EHEC_DB already EXISTS, Set Multiuser to Singleuser so all open connections are cut...';
	USE [master];
	-- Set DB toSingle User so open connections do not block Destroy of DB
	ALTER DATABASE EHEC_DB SET SINGLE_USER WITH ROLLBACK IMMEDIATE 
	PRINT 'DB will be deleted for a new creation.'
	DROP DATABASE EHEC_DB;
  END;
ELSE
  BEGIN;
	PRINT 'EHEC_DB does NOT EXISTS,  the db will be created now...';
  END;
GO

-- Datenbank erstellen:
IF db_id('EHEC_DB') is null
  BEGIN;
	CREATE DATABASE EHEC_DB;
  END;
ELSE
  BEGIN;
	PRINT 'EHEC_DB existiert bereits, Tabellen werden nun gel�scht...';
  END;
GO

SET QUOTED_IDENTIFIER OFF;
GO
USE [EHEC_DB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
PRINT 'Dropping existing FOREIGN KEY constraints';
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ResultExam]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Exam] DROP CONSTRAINT [FK_ResultExam];
GO
IF OBJECT_ID(N'[dbo].[FK_PatientExam]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Exam] DROP CONSTRAINT [FK_PatientExam];
GO
IF OBJECT_ID(N'[dbo].[FK_DoctorExam]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Exam] DROP CONSTRAINT [FK_DoctorExam];
GO

IF OBJECT_ID(N'[dbo].[FK_Exam]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Origin_Exam] DROP CONSTRAINT [FK_Exam];
GO
IF OBJECT_ID(N'[dbo].[FK_Origin]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Origin_Exam] DROP CONSTRAINT [FK_Origin];
GO
-- --------------------------------------------------
PRINT 'Dropping existing TABLES';
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Patient]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Patient];
GO
IF OBJECT_ID(N'[dbo].[Doctor]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Doctor];
GO
IF OBJECT_ID(N'[dbo].[Origin_Exam]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Origin_Exam];
GO
IF OBJECT_ID(N'[dbo].[Origin]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Origin];
GO
IF OBJECT_ID(N'[dbo].[Exam]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Exam];
GO
IF OBJECT_ID(N'[dbo].[Result]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Result];
GO

-- --------------------------------------------------
PRINT 'Creating all TABLES';
-- --------------------------------------------------

-- Creating table 'Patient'
CREATE TABLE [dbo].[Patient] (
    [PatientId] int IDENTITY(1,1) NOT NULL,
	[PatientUid] nvarchar(max) NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [BirthDate] datetime  NOT NULL,
    [Street] nvarchar(max)  NOT NULL,
    [City] nvarchar(max)  NOT NULL,
	[Region] nvarchar(max) NOT NULL,
	CONSTRAINT [PK_Patient]
    PRIMARY KEY CLUSTERED ([PatientId] ASC)
);
GO

-- Creating table 'Doctor'
CREATE TABLE [dbo].[Doctor] (
    [DoctorId] int IDENTITY(1,1) NOT NULL,
	[DoctorUid] nvarchar(max) NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
	[DoctorOrigin] nvarchar(max) NOT NULL,
	[Region] nvarchar(max) NOT NULL,
	CONSTRAINT [PK_Doctor]
    PRIMARY KEY CLUSTERED ([DoctorId] ASC)
);
GO

-- Create Table City
CREATE TABLE City(
	CityId int IDENTITY(1,1) NOT NULL,
	CityUid nvarchar(max) NOT NULL,
	CityName nvarchar(max) NOT NULL,
	CONSTRAINT PK_CityId
	PRIMARY KEY CLUSTERED (CityId ASC)
);
GO
-- Creating table 'Origin'
CREATE TABLE [dbo].[Origin] (
    [OriginId] int IDENTITY(1,1) NOT NULL,
	[OriginUid] nvarchar(max) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
	[Food] nvarchar(max) NOT NULL,
    [Street] nvarchar(max)  NOT NULL,
    [CityId] int NOT NULL,
	CONSTRAINT [PK_Origin]
    PRIMARY KEY CLUSTERED ([OriginId] ASC),
	CONSTRAINT FK_CityId
	FOREIGN KEY (CityId) REFERENCES City(CityId)
);
GO

-- Creating table 'Result'
CREATE TABLE [dbo].[Result] (
    [ResultId] int IDENTITY(1,1) NOT NULL,
	[ResultUid] nvarchar(max) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
	CONSTRAINT [PK_Result]
    PRIMARY KEY CLUSTERED ([ResultId] ASC)
);
GO

-- Creating table 'Exam'
CREATE TABLE [dbo].[Exam] (
    [ExamId] int IDENTITY(1,1) NOT NULL,
	[ExamUid] nvarchar(max) NOT NULL,
    [DoctorId] int  NOT NULL,
	[PatientId] int NOT NULL,
    [ResultId] int  NOT NULL,
	CONSTRAINT [PK_Exam]
    PRIMARY KEY CLUSTERED ([ExamId] ASC),
	CONSTRAINT [FK_ResultExam]
    FOREIGN KEY ([ResultId])
    REFERENCES [dbo].[Result]([ResultId])
    ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT [FK_DoctorExam]
    FOREIGN KEY ([DoctorId])
    REFERENCES [dbo].[Doctor]([DoctorId])
    ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT [FK_PatientExam]
    FOREIGN KEY ([PatientId])
    REFERENCES [dbo].[Patient]([PatientId])
    ON DELETE NO ACTION ON UPDATE NO ACTION
);
GO	

-- Creating table 'Origin_Exam'
CREATE TABLE [dbo].[Origin_Exam] (
    [Origin_ExamId] int IDENTITY(1,1) NOT NULL,
	[Origin_ExamUid] nvarchar(max) NOT NULL,
    [OriginId] int  NOT NULL,
	[ExamId] int NOT NULL,
	CONSTRAINT [PK_Origin_Exam]
    PRIMARY KEY CLUSTERED ([Origin_ExamId] ASC),
	CONSTRAINT [FK_Origin]
    FOREIGN KEY ([OriginId])
    REFERENCES [dbo].[Origin] ([OriginId])
    ON DELETE NO ACTION ON UPDATE NO ACTION,
	CONSTRAINT [FK_Exam]
    FOREIGN KEY ([ExamId])
    REFERENCES [dbo].[Exam]([ExamId])
    ON DELETE NO ACTION ON UPDATE NO ACTION
);
GO

-- --------------------------------------------------
PRINT 'Creating all INDEXES';
-- --------------------------------------------------
-- Creating non-clustered index for FOREIGN KEY 'FK_ResultExam'
CREATE INDEX [IX_FK_ResultExam]
ON [dbo].[Exam]
    ([ResultId]);
GO
-- Creating non-clustered index for FOREIGN KEY 'FK_DoctorExam'
CREATE INDEX [IX_FK_DoctorExam]
ON [dbo].[Exam]
    ([DoctorId]);
GO
-- Creating non-clustered index for FOREIGN KEY 'FK_DoctorExam'
CREATE INDEX [IX_FK_PatientExam]
ON [dbo].[Exam]
    ([PatientId]);
GO
-- Creating non-clustered index for FOREIGN KEY 'FK_Origin_Exam'
CREATE INDEX [IX_FK_Origin]
ON [dbo].[Origin_Exam]
    ([OriginId]);
GO
-- Creating non-clustered index for FOREIGN KEY 'FK_Origin_Exam'
CREATE INDEX [IX_FK_Exam]
ON [dbo].[Origin_Exam]
    ([ExamId]);
GO

-- --------------------------------------------------
IF DATABASEPROPERTYEX('EHEC_DB','UserAccess') = 'SINGLE_USER'
	begin;
		PRINT 'Set Singleuser mode back to Multiuser mode.';
		-- set DB back to Multiuser so multiconnections are alowed again.
		ALTER DATABASE EHEC_DB SET MULTI_USER
	end;
-- --------------------------------------------------
PRINT 'Successfull created your DB!';
-- Script has ended
-- --------------------------------------------------